FROM openjdk:11

WORKDIR /app

COPY ./mvnw .
RUN chmod +x ./mvnw

COPY . .
RUN ./mvnw package

CMD ["java", "-jar", "target/main-0.0.1-SNAPSHOT.jar"]
