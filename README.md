# Lab 1 -- Introduction to the quality gates

[![pipeline status](https://gitlab.com/danmory/s23-lab-1-introduction-to-the-quality-gates/badges/main/pipeline.svg)](https://gitlab.com/danmory/s23-lab-1-introduction-to-the-quality-gates/-/commits/main)

## Lab

Pipelines are configured.

## Homework

The server is running in **Innopolis Network**!

Server address: 10.90.137.107:8080

Hello Endpoint: 10.90.137.107:8080/hello

The image demonstrates that server works:

![out](assets/proof.png)
